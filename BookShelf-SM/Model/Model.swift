//
//  Model.swift
//  BookShelf-SM
//
//  Created by Samat Murzaliev on 07.05.2022.
//

import Foundation

class Model {
    private var books: [Books] = [Books(coverImage: "potter2", title: "Harry Potter and the Chamber of Secrets", rating: 4, description: "Harry Potter's summer has included the worst birthday ever, doomy warnings from a house-elf called Dobby, and rescue from the Dursleys by his friend Ron Weasley in a magical flying car! Back at Hogwarts School of Witchcraft and Wizardry for his second year, Harry hears strange whispers echo through empty corridors - and then the attacks start. Students are found as though turned to stone... Dobby's sinister predictions seem to be coming true."),
    Books(coverImage: "potter3", title: "Harry Potter and the Prisoner of Azkoban", rating: 5, description: "When the Knight Bus crashes through the darkness and screeches to a halt in front of him, it's the start of another far from ordinary year at Hogwarts for Harry Potter. Sirius Black, escaped mass-murderer and follower of Lord Voldemort, is on the run - and they say he is coming after Harry. In his first ever Divination class, Professor Trelawney sees an omen of death in Harry's tea leaves... But perhaps most terrifying of all are the Dementors patrolling the school grounds, with their soul-sucking kiss..."),
    Books(coverImage: "heritage", title: "Heritage", rating: 3, description: "Sean Brock is the chef behind the game-changing restaurants Husk and McCrady’s, and his first book offers all of his inspired recipes. With a drive to preserve the heritage foods of the South, Brock cooks dishes that are ingredient-driven and reinterpret the flavors of his youth in Appalachia and his adopted hometown of Charleston. The recipes include all the comfort food (think food to eat at home) and high-end restaurant food (fancier dishes when there’s more time to cook) for which he has become so well-known. Brock’s interpretation of Southern favorites like Pickled Shrimp, Hoppin’ John, and Chocolate Alabama Stack Cake sit alongside recipes for Crispy Pig Ear Lettuce Wraps, Slow-Cooked Pork Shoulder with Tomato Gravy, and Baked Sea Island Red Peas. This is a very personal book, with headnotes that explain Brock’s background and give context to his food and essays in which he shares his admiration for the purveyors and ingredients he cherishes."),
//    Books(coverImage: "1984", title: "1984", rating: 4, description: "Winston Smith toes the Party line, rewriting history to satisfy the demands of the Ministry of Truth. With each lie he writes, Winston grows to hate the Party that seeks power for its own sake and persecutes those who dare to commit thoughtcrimes. But as he starts to think for himself, Winston can’t escape the fact that Big Brother is always watching..."),
    Books(coverImage: "richDad", title: "Rich Dad Poor Dad", rating: 4, description: "Rich Dad Poor Dad is Robert's story of growing up with two dads — his real father and the father of his best friend, his rich dad — and the ways in which both men shaped his thoughts about money and investing. The book explodes the myth that you need to earn a high income to be rich and explains the difference between working for money and having your money work for you.")]
    
    private var bookCovers: [String: String] = ["Harry Potter and the Chamber of Secrets" : "potter2", "Harry Potter and the Prisoner of Azkoban": "potter3", "Heritage": "heritage", "1984": "1984", "Rich Dad Poor Dad": "richDad", "Dune": "dune"]
    private weak var controller: MainController?
    
    
    init(controller: MainController) {
        self.controller = controller
    }
    
    func getAllBooksFromModel() -> [Books] {
        return books
    }
    
    func saveNewBookToModel(_ newBook: Books) {
        books.insert(newBook, at: 0)
        print("All Books from model: \(books)")
    }
    
    func getBookCover(_ title: String) -> String {
        return bookCovers[title] ?? "default"
    }
    
}

struct Books {
    let coverImage: String
    let title: String
    let rating: Int
    let description: String
}
