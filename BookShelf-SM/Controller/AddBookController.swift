//
//  AddBookController.swift
//  BookShelf-SM
//
//  Created by Samat Murzaliev on 08.05.2022.
//

import Foundation

class AddBookController {
    private weak var view: AddBookView?
    private var model: Model!
    
    init(view: AddBookView) {
        self.view = view
        self.model = Model(controller: self)
    }
    
    func getAllBooks() -> [Books] {
        return model.getAllBooksFromModel()
    }
    
    func saveNewBook(newBook: Books) {
        model.saveNewBookToModel(newBook)
    }
    
    func getBookCover(title: String) -> String {
        return "default"
    }
}
