//
//  MainController.swift
//  BookShelf-SM
//
//  Created by Samat Murzaliev on 07.05.2022.
//

import Foundation
import UIKit

class MainController {
    private weak var view: UIViewController?
    private var model: Model!
    
    init(view: UIViewController) {
        self.view = view
        self.model = Model(controller: self)
    }
    
    func getAllBooks() -> [Books] {
        return model.getAllBooksFromModel()
    }
    
    func saveNewBook(newBook: Books) {
        model.saveNewBookToModel(newBook)
    }
    
    func getBookCover(title: String) -> String {
        model.getBookCover(title)
    }
}
