//
//  MainView.swift
//  BookShelf-SM
//
//  Created by Samat Murzaliev on 07.05.2022.
//

import UIKit

class MainView: UIViewController {
    
    private var controller: MainController?
    
    private var addBookVC = AddBookView()
    
    private lazy var bookTable: UITableView = {
        let view = UITableView()
        view.dataSource = self
        view.delegate = self
        view.backgroundColor = .lightGray
        return view
    }()
    
    private lazy var backImage: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "background")
        view.contentMode = .scaleAspectFill
        view.alpha = 0.6
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        controller = MainController(view: self)
        addBookVC.delegate = self
        title = "BookShelf"
        navigationController?.navigationBar.tintColor = .systemBlue
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addTapped))
        setSubviews()
    }
    
    @objc func addTapped() {
        navigationController?.pushViewController(addBookVC, animated: true)
    }
    
    private func setSubviews() {
        view.addSubview(backImage)
        backImage.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
                
        view.addSubview(bookTable)
        bookTable.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.bottom.equalTo(view.safeArea.bottom)
            make.right.left.equalToSuperview()
        }
    }
}

extension MainView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return controller?.getAllBooks().count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = BookShelfCell()
        cell.fill(book: (controller?.getAllBooks()[indexPath.row])!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}

extension MainView: AddBookDelegate {
    func saveNewBook(book: Books) {
        controller?.saveNewBook(newBook: book)
        self.bookTable.reloadData()
    }
}
