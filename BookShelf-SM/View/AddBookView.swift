//
//  AddBookView.swift
//  BookShelf-SM
//
//  Created by Samat Murzaliev on 07.05.2022.
//

import UIKit
import SnapKit

protocol AddBookDelegate {
    func saveNewBook(book: Books)
}

class AddBookView: UIViewController {
    
    private var controller: MainController?
    
    var delegate: AddBookDelegate?
    
    private lazy var backImage: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "background")
        view.contentMode = .scaleAspectFill
        view.alpha = 0.6
        return view
    }()
    
    private lazy var bookCover: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "default")
        
        return view
    }()
    
    private lazy var bookTitle: UITextField = {
        let view = UITextField()
        view.font = .systemFont(ofSize: 14, weight: .regular)
        view.placeholder = "Enter book title"
        view.textColor = .black
        view.borderStyle = .roundedRect
        view.backgroundColor = .lightGray
        view.delegate = self
        view.tag = 1
        view.autocapitalizationType = .none
        
        return view
    }()
    
    private lazy var bookRatingLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 14, weight: .bold)
        view.textColor = .black
        view.text = "Book Rating: 3"
        return view
    }()
    
    private lazy var bookRating: UISlider = {
        let view = UISlider()
        view.minimumValue = 1
        view.maximumValue = 5
        view.value = 3
        view.addTarget(self, action: #selector(ratingChanged(view:)), for: .valueChanged)
        return view
    }()
    
    private lazy var bookDescription: UITextView = {
        let view = UITextView()
        view.font = .systemFont(ofSize: 14, weight: .regular)
        view.textColor = .black
        view.backgroundColor = .lightGray
        
        return view
    }()
    
    private lazy var saveButton: UIButton = {
        let view = UIButton(type: .system)
        let config = UIButton.Configuration.filled()
        view.configuration = config
        view.setTitle("Save book", for: .normal)
        view.addTarget(self, action: #selector(saveBook(view:)), for: .touchUpInside)
        
        return view
    }()
    
    @objc func ratingChanged(view: UISlider) {
        bookRatingLabel.text = "Book Rating: \(Int(view.value))"
    }
    
    @objc func saveBook(view: UIButton) {
        let newBook = Books(coverImage: (controller?.getBookCover(title: bookTitle.text!))!, title: bookTitle.text!, rating: Int(bookRating.value), description: bookDescription.text!)
        self.delegate?.saveNewBook(book: newBook)
        navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        controller = MainController(view: self)
        setSubviews()
    }
    
    private func setSubviews() {
        view.addSubview(backImage)
        backImage.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        view.addSubview(bookCover)
        bookCover.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.height.equalToSuperview().dividedBy(4)
            make.width.equalToSuperview().dividedBy(3)
            make.top.equalTo(view.safeArea.top).offset(10)
        }
        
        view.addSubview(bookTitle)
        bookTitle.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.width.equalToSuperview().dividedBy(1.4)
            make.height.equalTo(40)
            make.top.equalTo(bookCover.snp.bottom).offset(20)
        }
        
        view.addSubview(bookRatingLabel)
        bookRatingLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(bookTitle.snp.bottom).offset(20)
        }
        
        view.addSubview(bookRating)
        bookRating.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.width.equalToSuperview().dividedBy(2.2)
            make.height.equalTo(30)
            make.top.equalTo(bookRatingLabel.snp.bottom).offset(10)
        }
        
        view.addSubview(bookDescription)
        bookDescription.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(bookRating.snp.bottom).offset(20)
            make.width.equalToSuperview().dividedBy(1.4)
            make.height.equalToSuperview().dividedBy(4)
        }
        
        view.addSubview(saveButton)
        saveButton.snp.makeConstraints { make in
            make.height.equalToSuperview().dividedBy(20)
            make.width.equalToSuperview().dividedBy(3)
            make.centerX.equalToSuperview()
            make.top.equalTo(bookDescription.snp.bottom).offset(30)
        }
    }
    
}

extension AddBookView: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 1 {
            let bookCoverImage = (controller?.getBookCover(title: textField.text!))
            bookCover.image = UIImage(named: bookCoverImage ?? "default")
        }
    }
}
