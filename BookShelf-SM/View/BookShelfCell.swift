//
//  BookShelfCell.swift
//  BookShelf-SM
//
//  Created by Samat Murzaliev on 07.05.2022.
//

import UIKit
import SnapKit

class BookShelfCell: UITableViewCell {
    
    let backImage: UIImageView = {
        let view = UIImageView(image: UIImage(named: "background"))
        view.contentMode = .scaleToFill
        view.alpha = 0.6
        return view
    }()
    
    let bookCover: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        
        return view
    }()
    
    let bookTitle: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 14, weight: .medium)
        view.textColor = .systemBlue
        
        return view
    }()
    
    let bookRating: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 14, weight: .medium)
        
        return view
    }()
    
    let bookDescription: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 12, weight: .thin)
        view.textColor = .black
        view.numberOfLines = 0
        view.textAlignment = .justified
        view.lineBreakMode = .byTruncatingTail
        view.sizeToFit()
        return view
    }()

    override func layoutSubviews() {
        
        addSubview(backImage)
        backImage.snp.makeConstraints { make in
            make.top.left.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(-5)
        }
        
        addSubview(bookCover)
        bookCover.snp.makeConstraints { make in
            make.height.equalTo(120)
            make.width.equalTo(60)
            make.top.equalToSuperview().offset(5)
            make.left.equalToSuperview().offset(5)
        }
        
        addSubview(bookTitle)
        bookTitle.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.height.equalTo(20)
            make.left.equalTo(bookCover.snp.right).offset(10)
            make.right.equalToSuperview().offset(-10)
        }
        addSubview(bookRating)
        bookRating.snp.makeConstraints { make in
            make.top.equalTo(bookTitle.snp.bottom).offset(2)
            make.height.equalTo(15)
            make.left.equalTo(bookCover.snp.right).offset(10)
        }
        
        addSubview(bookDescription)
        bookDescription.snp.makeConstraints { make in
            make.left.equalTo(bookCover.snp.right).offset(10)
            make.top.equalTo(bookRating.snp.bottom).offset(3)
            make.right.equalToSuperview().offset(-10)
            make.bottom.equalToSuperview().offset(-10)
        }
    }
    
    func fill(book: Books) {
        bookCover.image = UIImage(named: book.coverImage)
        bookTitle.text = book.title
        bookRating.text = "Book Rating: \(book.rating)"
        bookDescription.text = book.description
    }

}
